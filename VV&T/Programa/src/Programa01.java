import java.util.InputMismatchException;
import java.util.Scanner;


public class Programa01 {

	public static void main (String[] args) {
		
// ** CASOS DE TESTE **  
//		<(1, ‘a’, ‘c’) ’não encontrado’>
//		<(1, ‘a’, ‘a’) ’0’>
//		<(5, ‘abcde’, ‘a’) ’0’>
//		<(5, ‘abcde’, ‘a, b, e’) ’0, 1, 4’>
//		<(5, ‘abcde’, ‘a, b, f’) ’0, 1, não encontrado’>
//		<(5, ‘abcd’, ‘a, b, f’) ’Entrada de caracteres incorreta’>
//
//		<(0, ‘s’, ‘s’) ‘Quantidade Incorreta’>
//		<(-1, ‘c’, ‘c’) ‘Quantidade Incorreta’>
//		<(1, ‘’, ‘c’) ’Entrada inválida’>
//		<(1, ‘cc’, ‘c’) ’Entrada inválida’>
//		<(21, ‘b’, ‘b’) ‘Quantidade Incorreta’>
		
		Scanner in = new Scanner(System.in);
		int qnt = 0;
		
		try {
			System.out.println("Digite a quantidade de caracteres: ");
			qnt = in.nextInt();	
			
			if (qnt < 1 || qnt > 20) {
				System.out.println("Quantidade de caracteres incorreta.");
				System.exit(1);
			} 
			
		} catch (InputMismatchException e) {
			System.out.println("Entrada Inválida");
		}
		
		System.out.println("Digite a cadeia de caracteres do tamanho especificado acima: ");
		String strCompleta = in.next();
		
		if (strCompleta.length() != qnt) {
			System.out.println("Quantidade de caracteres incorreta.");
			System.exit(1);
		}
		
		System.out.println("Digite os caracteres a buscar: ");
		String strBusca = in.next();
		
		if (strBusca.length() == 0) {
			System.out.println("Quantidade de caracteres de busca incorreta.");
			System.exit(1);
		}
		
		char caracteres[] = new char[strBusca.toCharArray().length];
		caracteres = strBusca.toCharArray();
		
		for (int i = 0; i < caracteres.length; i++) {
			System.out.println("Caracter " + caracteres[i] + " encontrado na posição " + strCompleta.indexOf(caracteres[i]));
		}
		
	}
	
}
