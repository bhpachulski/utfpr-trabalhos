package pack;

public class TesteJabuti {
	
	public static int lastZero(int[] x) {
		// se x==0 lança exceção
		// senão retorna a posição do ultimo 0 em x
		// senão retorna 1 caso não existem 0s
		for (int i = 0; i < x.length; i++) {
			if (x[i] == 0)
				return i;
		}
		return -1;
	}

}
